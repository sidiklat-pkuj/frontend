import { Actionkeuangan } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [Actionkeuangan.postkeuangan]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.post(url+'/keuangan',formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionkeuangan.getAllkeuangan]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/keuangan/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionkeuangan.putkeuangan]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.put(url+'/keuangan/'+isi.id,formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionkeuangan.delkeuangan]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/keuangan/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}