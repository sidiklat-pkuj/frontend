import { createStore } from "vuex";
import bootstrap from "bootstrap/dist/js/bootstrap.min.js";
import auth from './auth';
import manajemen_user from './manajemen_user';
// import manajemen_peserta from "./manajemen_peserta";
import master_reward from "./master_reward";
import undian from "./undian";
import manajemen_pelatihan from "./manajemen_pelatihan";
import list_menu from "./list_menu";
import sub_menu from "./sub_menu";
import narasumber from "./narasumber";
import peserta from "./peserta";
import mot from "./mot";
import kurikulum from "./kurikulum";
import dokumentasi from "./dokumentasi";
import sertifikat from "./sertifikat";
import masterjenis from "./masterjenis";
import surat from "./surat";
import keuangan from "./keuangan";

export default createStore({
  modules: {
    auth: auth,
    manajemen_user: manajemen_user,
    // manajemen_peserta: manajemen_peserta,
    master_reward:master_reward,
    undian:undian,
    manajemen_pelatihan:manajemen_pelatihan,
    list_menu:list_menu,
    sub_menu:sub_menu,
    narasumber:narasumber,
    peserta:peserta,
    mot:mot,
    kurikulum:kurikulum,
    dokumentasi:dokumentasi,
    sertifikat:sertifikat,
    masterjenis:masterjenis,
    surat:surat,
    keuangan:keuangan
  },
  state: {
    hideConfigButton: false,
    isPinned: true,
    showConfig: false,
    isTransparent: "",
    isRTL: false,
    color: "",
    isNavFixed: false,
    isAbsolute: false,
    showNavs: true,
    showSidenav: true,
    showNavbar: true,
    showFooter: true,
    showMain: true,
    navbarFixed:
      "position-sticky blur shadow-blur left-auto top-1 z-index-sticky px-0 mx-4",
    absolute: "position-absolute px-4 mx-0 w-100 z-index-2",
    bootstrap,
  },
  mutations: {
    toggleConfigurator(state) {
      state.showConfig = !state.showConfig;
    },
    navbarMinimize(state) {
      const sidenav_show = document.querySelector(".g-sidenav-show");
      if (sidenav_show.classList.contains("g-sidenav-hidden")) {
        sidenav_show.classList.remove("g-sidenav-hidden");
        sidenav_show.classList.add("g-sidenav-pinned");
        state.isPinned = true;
      } else {
        sidenav_show.classList.add("g-sidenav-hidden");
        sidenav_show.classList.remove("g-sidenav-pinned");
        state.isPinned = false;
      }
    },
    sidebarType(state, payload) {
      state.isTransparent = payload;
    },
    cardBackground(state, payload) {
      state.color = payload;
    },
    navbarFixed(state) {
      if (state.isNavFixed === false) {
        state.isNavFixed = true;
      } else {
        state.isNavFixed = false;
      }
    },
    toggleEveryDisplay(state) {
      state.showNavbar = !state.showNavbar;
      state.showSidenav = !state.showSidenav;
      state.showFooter = !state.showFooter;
    },
    toggleHideConfig(state) {
      state.hideConfigButton = !state.hideConfigButton;
    },
  },
  actions: {
    toggleSidebarColor({ commit }, payload) {
      commit("sidebarType", payload);
    },
    setCardBackground({ commit }, payload) {
      commit("cardBackground", payload);
    },
  },
  getters: {},
});
