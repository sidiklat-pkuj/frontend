import { Actionmasterjenis } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [Actionmasterjenis.postmasterjenis]({commit},isi) {
        return new Promise((resolve, reject) => {
            

            Axios.post(url+'/masterjenis',isi, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionmasterjenis.getAllmasterjenis]({commit}) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/masterjenis/', {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionmasterjenis.putmasterjenis]({commit},isi) {
        return new Promise((resolve, reject) => {
         

            Axios.put(url+'/masterjenis/'+isi.id,isi, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionmasterjenis.delmasterjenis]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/masterjenis/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}