import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "@/views/Dashboard.vue";
import Tables from "@/views/Tables.vue";
// import Billing from "@/views/Billing.vue";
// import VirtualReality from "@/views/VirtualReality.vue";
// import Profile from "@/views/Profile.vue";
// import Rtl from "@/views/Rtl.vue";
import SignIn from "@/views/SignIn.vue";
// import SignUp from "@/views/SignUp.vue";
import TableUser from "@/views/admin/TableUser.vue";
import TablePelatihan from "@/views/admin/TablePelatihan.vue";
// import TablePeserta from "@/views/user/TablePeserta.vue";
import TableNarasumber from "@/views/admin/TableNarasumber.vue";
import TablePeserta from "@/views/admin/TablePeserta.vue";
import TableMot from "@/views/admin/TableMot.vue";
import TableKurikulum from "@/views/admin/TableKurikulum.vue";
import TableDokumentasi from "@/views/admin/TableDokumentasi.vue";
import TableSertifikat from "@/views/admin/TableSertifikat.vue";
import TableMasterjenis from "@/views/admin/TableMasterjenis.vue";
import TableSurat from "@/views/admin/TableSurat.vue";
import TableKeuangan from "@/views/admin/TableKeuangan.vue";
import ListPelatihan from "@/views/admin/ListPelatihan.vue";
// import Undian from "@/views/user/Undian.vue";
// import MasterReward from "@/views/user/MasterReward.vue";
// import ListUndian from "@/views/components/ListUndian.vue";
// import ListUndianTablePeserta from "@/views/components/ListUndianTablePeserta.vue";
// import ListUndianTableReward from "@/views/components/ListUndianTableReward.vue";
import store  from '../store/index';
import { Actionauth, Mutationauth } from "../store/auth/types";

const routes = [
  {
    path: "/",
    name: "/",
    redirect: "/dashboard",
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/manajemenuser",
    name: "ManajemenUser",
    component: TableUser,
  },
  // {
  //   path: "/manajemenpeserta",
  //   name: "ManajemenPeserta",
  //   component: TablePeserta,
  // },
  {
    path: "/daftarpelatihan",
    name: "DaftarPelatihan",
    component: TablePelatihan,
  },
  {
    path: "/tablenarasumber",
    name: "TableNarasumber",
    component: TableNarasumber,
  },
  {
    path: "/tablepeserta",
    name: "TablePeserta",
    component: TablePeserta,
  },
  {
    path: "/tablemot",
    name: "TableMot",
    component: TableMot,
  },
  {
    path: "/tablekurikulum",
    name: "TableKurikulum",
    component: TableKurikulum,
  },{
    path: "/tablesertifikat",
    name: "TableSertifikat",
    component: TableSertifikat,
  },
  {
    path: "/tabledokumentasi",
    name: "TableDokumentasi",
    component: TableDokumentasi,
  },
  {
    path: "/tablemasterjenis",
    name: "TableMasterjenis",
    component: TableMasterjenis,
  },
  {
    path: "/tablesurat",
    name: "TableSurat",
    component: TableSurat,
  },
  {
    path: "/tablekeuangan",
    name: "TableKeuangan",
    component: TableKeuangan,
  },
  {
    path: "/listpelatihan",
    name: "ListPelatihan",
    component: ListPelatihan,
  },
  // {
  //   path: "/undian",
  //   name: "Undian",
  //   component: Undian,
  // },
  // {
  //   path: "/listundian",
  //   name: "ListUndian",
  //   component: ListUndian,
  // },
  // {
  //   path: "/listundiantablepeserta",
  //   name: "ListUndianTablePeserta",
  //   component: ListUndianTablePeserta,
  // },
  // {
  //   path: "/listundiantablereward",
  //   name: "ListUndianTableReward",
  //   component: ListUndianTableReward,
  // },
  {
    path: "/tables",
    name: "Tables",
    component: Tables,
  },
  // {
  //   path: "/billing",
  //   name: "Billing",
  //   component: Billing,
  // },
  // {
  //   path: "/virtual-reality",
  //   name: "Virtual Reality",
  //   component: VirtualReality,
  // },
  // {
  //   path: "/profile",
  //   name: "Profile",
  //   component: Profile,
  // },
  // {
  //   path: "/rtl-page",
  //   name: "Rtl",
  //   component: Rtl,
  // },
  {
    path: "/sign-in",
    name: "Sign In",
    component: SignIn,
  },
  // {
  //   path: "/sign-up",
  //   name: "Sign Up",
  //   component: SignUp,
  // },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  linkActiveClass: "active",
});

function onLinkClicked(){
  return new Promise((resolve,reject)=>{
    store
    .dispatch(`${Actionauth.cekuser}`).then(res=>{
      console.log(res);
      store.commit(`${[Mutationauth.setTokenVerif]}`,true);
      localStorage.setItem('verifToken',true)
      resolve(true)
    }).catch(err =>{
      console.log(err);
      store.commit(`${[Mutationauth.setTokenVerif]}`,false);
      localStorage.setItem('verifToken',false)
      reject(false);
    })
  })
}

router.afterEach(async ()=>{
  console.log(store.getters.getTokenVerif);
  await onLinkClicked();
})

router.beforeEach(async (to,form, next) => {
  console.log(store.getters.getTokenVerif);
  console.log(localStorage.getItem('verifToken'));
  if (to.name !== 'Sign In' && localStorage.getItem('verifToken') != 'true') next({ name: 'Sign In' })
  else next();
})


export default router;
