import { Actionauth, Mutationauth } from '../types'
import Axios from 'axios'

var url = "https://api.projekmhz.xyz"
export default {
    [Actionauth.Login]({commit}, dataUser) {
        return new Promise( (resolve, reject) => {
            console.log("sini3");
             Axios.post(url+'/api/auth/signin', {
                 "username":dataUser.username,
                 "password":dataUser.password
             },
            ).then(async resAuthSmart =>{
                console.log("siniiiiii");
                const token =  resAuthSmart.data.accessToken
                const user =  resAuthSmart.data.username
                const nama =  resAuthSmart.data.nama
                const idUser =  resAuthSmart.data.id
                const role =  resAuthSmart.data.roles[0]
                //(res.data);
                localStorage.setItem('token', token)
                localStorage.setItem('idUser', idUser)
                localStorage.setItem('role', role)
                localStorage.setItem('nama', nama)
                localStorage.setItem('username', user)
                localStorage.setItem('verifToken', true)
                commit(`${[Mutationauth.setTokenVerif]}`, true)
                commit(`${[Mutationauth.success]}`, 'success',token, user)
                resolve(resAuthSmart);
            }).catch(error =>{
                    localStorage.removeItem('token')
                reject(error);
            });

        });
    },

    [Actionauth.cekuser]() {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/api/cektoken', {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { reject(err) }
            )
        })
    },

    [Actionauth.parseJWT](token) {
        //('isi :',token)


        //return JSON.parse(jsonPayload)
        return token
    },

    [Actionauth.Logout]({ commit }) {
        return new Promise((resolve,reject)=>{
            try {
                localStorage.clear()
                commit(`${[Mutationauth.Islogin]}`, false)
                commit(`${[Mutationauth.setTokenVerif]}`, false)   
                resolve({response:"oke"});
            } catch (error) {
                reject(error)
            }
        });
        
    }

}
