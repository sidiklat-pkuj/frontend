import { Actiondokumentasi } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [Actiondokumentasi.postdokumentasi]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.post(url+'/dokumentasi',formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actiondokumentasi.getAlldokumentasi]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/dokumentasi/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actiondokumentasi.putdokumentasi]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.put(url+'/dokumentasi/'+isi.id,formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actiondokumentasi.deldokumentasi]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/dokumentasi/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}