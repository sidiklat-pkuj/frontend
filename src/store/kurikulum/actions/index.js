import { Actionkurikulum } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [Actionkurikulum.postkurikulum]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.post(url+'/kurikulum',formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionkurikulum.getAllkurikulum]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/kurikulum/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionkurikulum.putkurikulum]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.put(url+'/kurikulum/'+isi.id,formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionkurikulum.delkurikulum]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/kurikulum/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}