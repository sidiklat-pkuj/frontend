import { ActionMasterReward } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [ActionMasterReward.postReward]({commit},isi) {
        return new Promise((resolve, reject) => {
            var formdata = new FormData()
			formdata.append('foto', isi.filesFoto);
			formdata.append('nama_hadiah', isi.nama_hadiah);
			formdata.append('created_by', isi.created_by);
			formdata.append('judulUndianId', isi.judulUndianId);
			formdata.append('urutan_hadiah', isi.urutan_hadiah);
			formdata.append('qty', isi.qty);
            Axios.post(url+'/master_reward',formdata, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [ActionMasterReward.getAllReward]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/master_reward/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [ActionMasterReward.putReward]({commit},isi) {
        return new Promise((resolve, reject) => {
            var formdata = new FormData()
			formdata.append('foto', isi.filesFoto);
			formdata.append('nama_hadiah', isi.nama_hadiah);
			formdata.append('created_by', isi.created_by);
			formdata.append('judulUndianId', isi.judulUndianId);
			formdata.append('urutan_hadiah', isi.urutan_hadiah);
			formdata.append('qty', isi.qty);
			formdata.append('img', isi.img);
            Axios.put(url+'/master_reward/'+isi.id,formdata, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [ActionMasterReward.delReward]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/master_reward/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}