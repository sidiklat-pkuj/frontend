import { Actionsurat } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [Actionsurat.postsurat]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.post(url+'/surat',formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionsurat.getAllsurat]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/surat/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionsurat.putsurat]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.put(url+'/surat/'+isi.id,formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionsurat.delsurat]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/surat/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}