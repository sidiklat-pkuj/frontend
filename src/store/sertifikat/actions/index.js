import { Actionsertifikat } from '../types'
import { Mutationauth } from '../../auth/types';
import Axios from 'axios'

var url = "https://api.projekmhz.xyz/api"
export default {
    [Actionsertifikat.postsertifikat]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.post(url+'/sertifikat',formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionsertifikat.getAllsertifikat]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.get(url+'/sertifikat/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionsertifikat.putsertifikat]({commit},isi) {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            Object.entries(isi).forEach(([key, value]) => {
                formData.append(key, value);
            });

            Axios.put(url+'/sertifikat/'+isi.id,formData, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
    [Actionsertifikat.delsertifikat]({commit},id) {
        return new Promise((resolve, reject) => {
            Axios.delete(url+'/sertifikat/'+id, {
                headers: {
                    'x-access-token': localStorage.getItem('token')
                }
            }).then(res => { resolve(res) }
            ).catch(err => { 
                if(err.response.status == 401){
                    commit(`${[Mutationauth.setTokenVerif]}`,false);
                }
                reject(err) 
            }
            )
        })
    },
}